import React from 'react';
const realtimeUrl =
  'http://ec2-34-243-177-87.eu-west-1.compute.amazonaws.com:5002/places/trace';

function App() {
  const [places, setPlaces] = React.useState([]);

  const handleMessage = (e) => {
    console.log('On message', e.data);
    setPlaces((prevState) => [...prevState, JSON.parse(e.data)]);
  };

  React.useEffect(() => {
    let eventSource = new EventSource(realtimeUrl);
    eventSource.onmessage = handleMessage;

    setInterval(() => {
      if (eventSource.readyState === EventSource.CLOSED) {
        eventSource = new EventSource(realtimeUrl);
        eventSource.onmessage = handleMessage;
      }
    }, 1000);
  }, []);

  return (
    <table id="places">
      <thead>
        <tr>
          <th>Place id</th>
          <th>Owner</th>
          <th>Status</th>
        </tr>
      </thead>

      <tbody>
        {places.map((place) => {
          return (
            <tr key={place?.placeKey}>
              <td>{place.placeKey}</td>
              <td>{place.actor2}</td>
              <td>CREATED</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default App;
