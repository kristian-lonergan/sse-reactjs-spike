# [SPIKE] - Server side events (SSE) w/React

### Description

This is a very basic sample of SSE interaction between server and client (using react on client side)
[see simple non-React JS implementation here](https://bitbucket.org/kristian-lonergan/sse-spike/src/master/)
This is connected to live AWS service for places "http://ec2-34-243-177-87.eu-west-1.compute.amazonaws.com:5002/places/trace"

## How to run

First you need to install the package dependencies by running `npm install`
Once that is completed run `npm start`
Navigate to http://localhost:3000

## How to know if its working

Have http://localhost:3000 open and create a place for any product.

You should get a new row in the table with the email and place id of the newly created place appear(without having to refresh the page)

---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
